package com.bae.viewmodeltest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.bae.viewmodeltest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity()
{
    lateinit var mBinding: ActivityMainBinding
    lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = mBinding.root
        setContentView(view)
        mainActivityViewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        mBinding.countText.text = mainActivityViewModel.getCurrentCount().toString()
        mBinding.button.setOnClickListener {
            mBinding.countText.text = mainActivityViewModel.getUpdatedCount().toString()
        }
    }
}